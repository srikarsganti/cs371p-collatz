# CS371p: Object-Oriented Programming Collatz Repo

* Name: Srikar Ganti

* EID: sg49799

* GitLab ID: srikarsganti

* HackerRank ID: srikarsganti

* Git SHA: 6f550c70341826ab03ad0536fc83e498d91951aa

* GitLab Pipelines: https://gitlab.com/srikarsganti/cs371p-collatz/-/pipelines

* Estimated completion time: 5 hours

* Actual completion time: 7 hours

* Comments: Overall, a pretty fun and informative lab. Thank you!
