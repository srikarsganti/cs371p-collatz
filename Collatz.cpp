// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

//* global variable storing our lazy cache
int lazy_cache[1000000];
// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;
    //* simple check to see if i and j are in the bounds specified by the assignment
    assert(is_in_bounds(i));
    assert(is_in_bounds(j));
    int max_cycle_length = 0;
    //* in case i and j are not in ascending order...ensure that lo and hi are in ascending order
    int lo = min(i, j);
    int hi = max(i, j);
    for(int num = lo; num <= hi; num++) {
        //* check to see if we have a value stored in the cache, if not, call cycle_length() on num
        int curr_cycle_length = lazy_cache[num] == 0 ? cycle_length(num) : lazy_cache[num];
        //* store cycle_length in the cache
        lazy_cache[num] = curr_cycle_length;
        max_cycle_length = max(max_cycle_length, curr_cycle_length);
    }
    assert(max_cycle_length > 0);
    return make_tuple(i, j, max_cycle_length);
}

// ------------
// cycle_length
// ------------

int cycle_length(long long n) {
    assert(n > 0);
    int cycle_count = 1;
    while(n > 1) {
        if((n % 2) == 0)
            n /= 2;
        else
            n = (3 * n) + 1;
        //* have to make sure that n is in bounds of cache, and if it is, return the value in cache + current cycle count
        if(is_in_bounds(n) && lazy_cache[n] != 0) {
            return lazy_cache[n] + cycle_count;
        }
        ++cycle_count;
    }
    assert(cycle_count > 0);
    return cycle_count;
}

// ------------
// is_in_bounds
// ------------

bool is_in_bounds(long long n) {
    return n > 0 && n < 1000000;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
